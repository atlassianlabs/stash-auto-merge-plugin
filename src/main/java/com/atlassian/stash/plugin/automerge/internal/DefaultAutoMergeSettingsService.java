package com.atlassian.stash.plugin.automerge.internal;

import com.atlassian.fugue.Iterables;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.stash.plugin.automerge.AutoMergeSettingsService;
import com.atlassian.stash.repository.Branch;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.scm.git.GitAgent;
import com.atlassian.stash.scm.git.GitScm;
import com.atlassian.stash.user.Permission;
import com.atlassian.stash.user.PermissionValidationService;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.MapMaker;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

public class DefaultAutoMergeSettingsService implements AutoMergeSettingsService {

    private final GitAgent gitAgent;
    private final PluginSettings pluginSettings;
    private final PermissionValidationService permissionValidationService;

    private final ConcurrentMap<Integer, List<String>> cache = new MapMaker()
            .makeComputingMap(new Function<Integer, List<String>>() {
                @Override
                public List<String> apply(Integer repoId) {
                    Object branchNames = pluginSettings.get(Integer.toString(repoId));
                    return branchNames instanceof List ? (List<String>) branchNames : Collections.<String>emptyList();
                }
            });

    public DefaultAutoMergeSettingsService(GitAgent gitAgent, PluginSettingsFactory pluginSettingsFactory, PermissionValidationService permissionValidationService) {
        this.gitAgent = gitAgent;
        this.permissionValidationService = permissionValidationService;
        pluginSettings = pluginSettingsFactory.createSettingsForKey("com.atlassian.stash.plugin.auto-merge-plugin");
    }

    @Override
    public List<Branch> getAutoMergeBranches(Repository repository) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_READ);
        return resolveBranches(repository, cache.get(repository.getId()));
    }

    @Override
    public void setAutoMergeBranches(Repository repository, List<String> branchNames) {
        permissionValidationService.validateForRepository(repository, Permission.REPO_ADMIN);
        pluginSettings.put(Integer.toString(repository.getId()), branchNames);
        cache.remove(repository.getId());
    }

    private List<Branch> resolveBranches(final Repository repository, List<String> branchOrder) {
        if (!GitScm.ID.equals(repository.getScmId())) {
            return Collections.emptyList();
        }
        return Lists.newArrayList(Iterables.flatMap(branchOrder, new Function<String, Iterable<Branch>>() {
            public Option<Branch> apply(String branchName) {
                return Option.option(gitAgent.resolveBranch(repository, branchName));
            }
        }));
    }
}
