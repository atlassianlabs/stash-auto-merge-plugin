package com.atlassian.stash.plugin.automerge;

import com.atlassian.stash.repository.Branch;
import com.atlassian.stash.repository.Repository;

import java.util.List;

public interface AutoMergeSettingsService {

    List<Branch> getAutoMergeBranches(Repository repository);

    void setAutoMergeBranches(Repository repository, List<String> branchNames);

}
